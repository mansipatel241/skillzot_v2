<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap{
	
	public function _initAutoloader(){
		
		$autoloader = new Zend_Application_Module_Autoloader(array('namespace'=>'Skillzot_','basePath'=>dirname(__FILE__)));
		
		// Return to bootstrap resource registry
		return $autoloader;
	}
	
	protected function _initDoctype(){
		
		$this->bootstrap('view');
		$view = $this->getResource('view');
		$view->doctype('XHTML1_STRICT');
	}
}