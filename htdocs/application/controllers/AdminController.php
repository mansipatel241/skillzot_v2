<?php

class AdminController extends Zend_Controller_Action{
	
	public function init(){
		
		$this->_helper->layout()->setLayout("admin");
	}
	
	public function indexAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(isset($authUserNamespace->user_id) || $authUserNamespace->user_id!="")$this->_redirect("/admin/home");
		
		$this->_helper->layout()->setLayout("login");
		
		if($this->_request->isPost()){
		
			$username = $this->_request->getParam('username');
			$password = $this->_request->getParam('password');
			//mysql_connect("localhost", "root", "root"); // main site
			//mysql_connect("localhost", "pronto_skiilzot", "prontoskiilZOT"); // pronto
			//mysql_connect("localhost", "skillzot_test", "Skillzot2012");// slash skillzot
			$usernameescape =$username;
			$passwordescape = md5($password);
			$adminlogin = new Skillzot_Model_DbTable_Adminlogin();
			$user_row = $adminlogin->fetchRow($adminlogin->select()
											 ->where("admin_uname='$usernameescape' && admin_pwd='$passwordescape' && is_active=1"));

			if($user_row!="" && sizeof($user_row)>0){
			
				$authUserNamespace->user_id = $user_row->id;
				$authUserNamespace->user_name = $user_row->admin_uname;
				
				$this->view->msg = "";
				
				$this->_redirect('/admin/home');
			
			}else{
				$this->view->msg = "inactive";
			}
		}
	}
	
	public function homeAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$authUserNamespace->admin_page_title = "Home";
	}
	
	public function cmslistAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		
	}

	public function categoryAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->setLayout("admin");
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$authUserNamespace->admin_page_title = "Manage Category";
		$skillObj = new Skillzot_Model_DbTable_Skills();
		
		$records_per_page = $this->_request->getParam('shown');
		$this->view->records_per_page = $records_per_page;
		
		$searchText = addslashes($this->_request->getParam('searchtext'));
		$this->view->search_val = stripcslashes ($searchText);
		
		if($records_per_page==""){
			$records_per_page = $this->_request->getParam('getPageValue');
			$this->view->records_per_page = $records_per_page;
		}
		
		if($searchText!="")
		{
			$skillrowResult =$skillObj->fetchAll($skillObj->select()
							->from(array('s'=>DATABASE_PREFIX."master_skills"))
							->where("s.parent_skill_id='0' && s.skill_id!='0' && skill_depth='1' && s.skill_name like '%".$searchText."%'")
							->order(array("order_id asc")));
												
		}
		else
		{
			//echo "sdf";exit;
			$skillrowResult = $skillObj->fetchAll($skillObj->select()
							->from(array('s'=>DATABASE_PREFIX."master_skills"))
							->where("s.parent_skill_id='0' && s.skill_id!='0' && skill_depth='1'")
							->order(array("order_id asc")));
		
		}

			/*pagination code*/
		if (isset($skillrowResult) && $skillrowResult!="")
		{
			$this->view->skillrowResult = $skillrowResult;
		}
		
	
		$page = $this->_request->getParam('page',1);
		//$this->view->page = $page;
		if($records_per_page=="")$records_per_page = 10;
		$record_count = sizeof($skillrowResult);
		$paginator = Zend_Paginator::factory($skillrowResult);
		$paginator->setItemCountPerPage($records_per_page);
		$paginator->setCurrentPageNumber($page);
		$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
		$this->view->skillrowResult = $paginator;
		$page_number  = $record_count / 1;
		$page_number_last =  floor($page_number);
												   
	}
	
public function subcategoryAction(){
		
	
		
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			$this->_helper->layout()->setLayout("admin");
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$authUserNamespace->admin_page_title = "Manage Category";
		$skillObj = new Skillzot_Model_DbTable_Skills();
		
		$records_per_page = $this->_request->getParam('shown');
		if (isset($records_per_page) && $records_per_page!=""){
			$this->view->records_per_page = $records_per_page;
		}
		
		$searchText = addslashes($this->_request->getParam('searchtext'));
		if (isset($searchText) && $searchText!=""){
			$this->view->search_val = stripcslashes ($searchText);
		}
		
		if($searchText!="")
		{
			$skillrowResult =$skillObj->fetchAll($skillObj->select()
							->from(array('s'=>DATABASE_PREFIX."master_skills"))
							->where("s.parent_skill_id!='0' && s.skill_id!='0' && skill_depth='2' && s.skill_name like '%".$searchText."%'")
							->order(array("order_id asc")));
												
		}
		else
		{
			//echo "sdf";exit;
			$skillrowResult = $skillObj->fetchAll($skillObj->select()
							->from(array('s'=>DATABASE_PREFIX."master_skills"))
							->where("s.parent_skill_id!='0' && s.skill_id!='0' && skill_depth='2'")
							 ->order(array("order_id asc")));
		
		}
		if (isset($skillrowResult) && $skillrowResult!="")
		{
			$this->view->skillrowResult = $skillrowResult;
		}
			/*pagination code*/
			
		$page = $this->_request->getParam('page',1);
		//$this->view->page = $page;
		if($records_per_page=="")$records_per_page = 10;
		$record_count = sizeof($skillrowResult);
		$paginator = Zend_Paginator::factory($skillrowResult);
		$paginator->setItemCountPerPage($records_per_page);
		$paginator->setCurrentPageNumber($page);
		$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
		$this->view->skillrowResult = $paginator;
		$page_number  = $record_count / 1;
		$page_number_last =  floor($page_number);
	}
		
	public function skillsAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->setLayout("admin");
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$authUserNamespace->admin_page_title = "Manage Skill";
		$skillObj = new Skillzot_Model_DbTable_Skills();
		
		$records_per_page = $this->_request->getParam('shown');
		if (isset($records_per_page) && $records_per_page!=""){
			$this->view->records_per_page = $records_per_page;
		}
		
		$searchText = addslashes($this->_request->getParam('searchtext'));
		if (isset($searchText) && $searchText!=""){
			$this->view->search_val = stripcslashes ($searchText);
		}
		
		if($records_per_page==""){
			$records_per_page = $this->_request->getParam('getPageValue');
			$this->view->records_per_page = $records_per_page;
		}
		
		if($searchText!="")
		{
			$skillrowResult =$skillObj->fetchAll($skillObj->select()
							->from(array('s'=>DATABASE_PREFIX."master_skills"))
							->where("s.parent_skill_id!='0' && s.skill_id!='0' && s.skill_depth!='0' && skill_depth!='1' && s.skill_name like '%".$searchText."%'")
							->order(array("order_id asc")));
												
		}
		else
		{
			$skillrowResult = $skillObj->fetchAll($skillObj->select()
							->from(array('s'=>DATABASE_PREFIX."master_skills"))
							->where("s.parent_skill_id!='0' && s.skill_id!='0' && skill_depth!='0' && skill_depth!='1'")
							->order(array("order_id asc")));
	
		}

		if(isset($skillrowResult) && $skillrowResult!="")
		{
			$this->view->skillrowResult = $skillrowResult;
		}
			/*pagination code*/
		
		$page = $this->_request->getParam('page',1);
		//$this->view->page = $page;
		if($records_per_page=="")$records_per_page = 10;
		$record_count = sizeof($skillrowResult);
		$paginator = Zend_Paginator::factory($skillrowResult);
		$paginator->setItemCountPerPage($records_per_page);
		$paginator->setCurrentPageNumber($page);
		$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
		$this->view->skillrowResult = $paginator;
		$page_number  = $record_count / 1;
		$page_number_last =  floor($page_number);
												   
	}
	
	public function addfinalskillsAction()
	{
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$this->_helper->layout()->disableLayout();
		$skillObj = new Skillzot_Model_DbTable_Skills();
		
			if($this->_request->isPost()){
			
				if($this->_request->isXmlHttpRequest()){
				$category_id = $this->_request->getParam("category_id");
				$category_rows = $skillObj->fetchAll($skillObj->select()
															  ->from(array('s'=>DATABASE_PREFIX."master_skills"),array('s.skill_name','s.skill_id'))
															  ->where("s.parent_skill_id!='0' && s.skill_id!='0' && parent_skill_id='$category_id' && skill_depth='2'"));
				$i=0;
				$tags = "";
				foreach($category_rows as $e){
					$tags[$i]['skill_name'] = $e->skill_name;
					$tags[$i]['skill_id'] = $e->skill_id;
					//$arry[]=$tags;
					$i++;
				}
				echo json_encode($tags);exit;
				}
	  		}
	}
	
	public function addskillsAction(){
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$this->_helper->layout()->setLayout("lightbox");
		$skillObj = new Skillzot_Model_DbTable_Skills();
		
		$skillrowResult = $skillObj->fetchAll($skillObj->select()
							->from(array('s'=>DATABASE_PREFIX."master_skills"),array('s.skill_name','s.skill_id'))
							->where("s.parent_skill_id='0' && s.skill_id!='0'"));
		$this->view->maincategory = $skillrowResult;
		$id = $this->_request->getParam("id");
		$this->view->id = $id;
		
		if(isset($id) && $id > 0)
		{
			$fetch_data = $skillObj->fetchRow("skill_id='$id'");
			$this->view->category =$fetch_data ;
		}

		if(isset($id) && $id > 0)
		{
			$authUserNamespace->admin_page_title = "Update Skill";
		}else{
		    $authUserNamespace->admin_page_title = "Add Skill";
		}

		if($this->_request->isPost()){

			$category_id = $this->_request->getParam("Category_name");
			$subcategory_name = $this->_request->getParam("categories_sub");
			$skill_name = $this->_request->getParam("Skill_name");
			$skill_description = $this->_request->getParam("SkillDescription");
			$enable = $this->_request->getParam("Enable");
			$menuable = $this->_request->getParam("menuable");
			$ordernunber = $this->_request->getParam("OrderNumber");
			$hiddenid= $this->_request->getParam("hiddenvar");
			
			$urlName= $this->_request->getParam("urlName");
			$pageTitle= $this->_request->getParam("skillPagetitle");


			if($this->_request->isXmlHttpRequest()){

				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();

				if($category_id == "")$response["data"]["Category_name"] = "null";
				else $response["data"]["Category_name"] = "valid";
				
				if($skill_name == "")$response["data"]["Skill_name"] = "null";
				else $response["data"]["Skill_name"] = "valid";
				
//				if($subcategory_name == "")$response["data"]["categories_sub"] = "null";
//				else $response["data"]["categories_sub"] = "valid";
				
				if($skill_description == "")$response["data"]["SkillDescription"] = "null";
				else $response["data"]["SkillDescription"] = "valid";
				
				if($urlName == "")$response["data"]["urlName"] = "null";
				else $response["data"]["urlName"] = "valid";
				
				if($pageTitle == "")$response["data"]["skillPagetitle"] = "null";
				else $response["data"]["skillPagetitle"] = "valid";

//				if (isset($subcategory_name) && $subcategory_name!="")
//				{
					$SkillNameData= $skillObj->fetchRow("skill_id='$subcategory_name' && skill_id!=0");
					if (isset($SkillNameData) && sizeof($SkillNameData) > 0)
					{
						$parent_id = $SkillNameData->skill_id;
					}else{
						$parent_id = $category_id;
					
					}
						if(isset($id) && $id !="")
						{
							$order_exist = $skillObj->fetchAll("skill_id!='$hiddenid' && order_id='$ordernunber' && parent_skill_id!='0' && parent_skill_id=$parent_id");
						}else{
							$order_exist = $skillObj->fetchAll("order_id='$ordernunber' && parent_skill_id!='0' && parent_skill_id='$parent_id'");
						}
//				}
				if($ordernunber == "")$response["data"]["OrderNumber"] = "null";
				elseif(isset($order_exist) && sizeof($order_exist) > 0)$response["data"]["OrderNumber"] = "duplicate";
				else $response["data"]["OrderNumber"] = "valid";

				if($enable == "")$response["data"]["Enable"] = "null";
				else $response["data"]["Enable"] = "valid";
				
				if($menuable == "")$response["data"]["menuable"] = "null";
				else $response["data"]["menuable"] = "valid";

				if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";

				echo json_encode($response);

			}else{
				$SkillData= $skillObj->fetchRow("skill_id='$category_id'");
				$final_category_name =  $SkillData->skill_name;
				if (isset($subcategory_name) && $subcategory_name!="")
				{
					$SkillNameData= $skillObj->fetchRow("skill_id='$subcategory_name'");
					$parent_id = $SkillNameData->skill_id;
				}else{
					$parent_id = $category_id;
				}
				if (isset($subcategory_name) && $subcategory_name=="" && isset($category_id) && $category_id!="")
				{
					$data = array("skill_name"=>$skill_name,"skill_description"=>$skill_description,"parent_skill_id"=>$parent_id,"skill_depth"=>'2',"is_enabled"=>$enable,"order_id"=>$ordernunber,"is_skill"=>$menuable);
				}else{
					$data = array("skill_name"=>$skill_name,"skill_description"=>$skill_description,"parent_skill_id"=>$parent_id,"skill_depth"=>'3',"is_enabled"=>$enable,"order_id"=>$ordernunber,"is_skill"=>$menuable);
				}
				//print_r($data);exit;
				if(isset($id) && $id > 0){
					$skillObj->update($data,"skill_id='$id'");
					
					if ((isset($menuable) && $menuable=="0"))
					{
						if (isset($subcategory_name) && $subcategory_name!="")
						{
							$data2 = array("is_skill"=>'0');
							$skillObj->update($data2,"parent_skill_id='$subcategory_name'");
							
							$SkillNameData= $skillObj->fetchRow("skill_id='$subcategory_name'");
							$parent_id = $SkillNameData->skill_id;
							$skillObj->update($data2,"parent_skill_id='$parent_id'");
							
						}else{
							$data2 = array("is_skill"=>'0');
							$skillObj->update($data2,"parent_skill_id='$id'");
							
						}
					}else{
						if (isset($subcategory_name) && $subcategory_name!="")
						{
							//echo "in";
							$data2 = array("is_skill"=>'1');
							$skillObj->update($data2,"parent_skill_id='$subcategory_name'");
							
							$SkillNameData= $skillObj->fetchRow("skill_id='$subcategory_name'");
							$parent_id = $SkillNameData->skill_id;
							$skillObj->update($data2,"parent_skill_id='$parent_id'");
							
						}else{
							$data2 = array("is_skill"=>'1');
							$skillObj->update($data2,"parent_skill_id='$id'");
						}
					}
					if(isset($enable) && $enable=="0")
					{
						if (isset($subcategory_name) && $subcategory_name!="")
						{
							//echo "out";
							$data3 = array("is_enabled"=>'0');
							$skillObj->update($data3,"skill_id='$subcategory_name'");
							
							$SkillNameData= $skillObj->fetchRow("skill_id='$subcategory_name'");
							$parent_id = $SkillNameData->skill_id;
							$skillObj->update($data3,"skill_id='$parent_id'");
						}else{
							$data3 = array("is_enabled"=>'0');
							$skillObj->update($data3,"skill_id='$id'");
							
						}
					}else{
						if (isset($subcategory_name) && $subcategory_name!="")
						{
							//echo "out";
							$data3 = array("is_enabled"=>'1');
							$skillObj->update($data3,"skill_id='$subcategory_name'");
							
							$SkillNameData= $skillObj->fetchRow("skill_id='$subcategory_name'");
							$parent_id = $SkillNameData->skill_id;
							$skillObj->update($data3,"skill_id='$parent_id'");
						}else{
							$data3 = array("is_enabled"=>'1');
							$skillObj->update($data3,"skill_id='$id'");
						}
					}
					$authUserNamespace->status_message = "Skills has been updated successfully";
				}else{
					$skillObj->insert($data);
					$authUserNamespace->status_message = "Skills has been added successfully";
				}
				//echo "<script>parent.Mediabox.close();</script>";
				echo "<script>window.parent.location='". BASEPATH ."/admin/skills';</script>";
			}
		}

	}
	public function addcategoryAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$this->_helper->layout()->setLayout("lightbox");
		
		$id = $this->_request->getParam("id");
		$this->view->id = $id;
		
		$skillObj = new Skillzot_Model_DbTable_Skills();
		
		if(isset($id) && $id > 0)
		{
			$this->view->category = $skillObj->fetchRow("skill_id='$id'");
		}
		
		if(isset($id) && $id > 0)
		{
			$authUserNamespace->admin_page_title = "Update Category";
		}else{
		    $authUserNamespace->admin_page_title = "Add Category";
		}
		
		if($this->_request->isPost()){
		
			$category_name = $this->_request->getParam("CategoryName");
			$category_description = $this->_request->getParam("CategoryDescription");
			$enable = $this->_request->getParam("Enable");
			$menuable = $this->_request->getParam("menuable");
			$ordernunber = $this->_request->getParam("OrderNumber");
			$hiddenid= $this->_request->getParam("hiddenvar");
			if($this->_request->isXmlHttpRequest()){
				
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				
				$data = array("skill_name"=>$category_name);
				if(isset($id) && $id !="")
				{
					$category_exist = $skillObj->fetchAll("skill_name = '$category_name' && skill_id!='$hiddenid' && parent_skill_id='0'");
				}else {
					$category_exist = $skillObj->fetchAll("skill_name = '$category_name' && parent_skill_id='0'");
				}
				
				if($data["skill_name"] == "")$response["data"]["CategoryName"] = "null";
				elseif(!preg_match("/^[a-zA-Z0-9 ]*$/", $data['skill_name']))$response['data']['CategoryName'] = "invalid";
				elseif(isset($category_exist) && sizeof($category_exist) > 0)$response["data"]["CategoryName"] = "duplicate";
				else $response["data"]["CategoryName"] = "valid";
				
				if($category_description == "")$response["data"]["CategoryDescription"] = "null";
				else $response["data"]["CategoryDescription"] = "valid";
				if(isset($id) && $id !="")
				{
					$order_exist = $skillObj->fetchAll("skill_id!='$hiddenid' && order_id='$ordernunber' && parent_skill_id='0'");
				}else {
					$order_exist = $skillObj->fetchAll("order_id='$ordernunber' && parent_skill_id='0'");
				}
								
				if($ordernunber == "")$response["data"]["OrderNumber"] = "null";
				elseif(isset($order_exist) && sizeof($order_exist) > 0)$response["data"]["OrderNumber"] = "duplicate";
				else $response["data"]["OrderNumber"] = "valid";
				
				if($enable == "")$response["data"]["Enable"] = "null";
				else $response["data"]["Enable"] = "valid";
				
				if($menuable == "")$response["data"]["menuable"] = "null";
				else $response["data"]["menuable"] = "valid";
				
				if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";
				
				echo json_encode($response);
				
			}else{
				$data = array("skill_name"=>$category_name,"skill_description"=>$category_description,"parent_skill_id"=>'0',"skill_depth"=>'1',"is_enabled"=>$enable,"is_skill"=>$menuable,"order_id"=>$ordernunber);
				if(isset($id) && $id > 0){
					$skillObj->update($data,"skill_id='$id'");
					if ((isset($menuable) && $menuable=="0"))
					{
							$data2 = array("is_skill"=>'0');
							$skillObj->update($data2,"parent_skill_id='$hiddenid'");
							
							
							$categoryskillRows = $skillObj->fetchAll($skillObj->select()
										  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
										  ->where("parent_skill_id = '$hiddenid'"));

							if (isset($categoryskillRows) && sizeof($categoryskillRows) > 0)
							{
								foreach($categoryskillRows as $idsname)
								{
									$skillObj->update($data2,"parent_skill_id='$idsname->skill_id'");
								}
							}
					
					}else{
							$data2 = array("is_skill"=>'1');
							$skillObj->update($data2,"parent_skill_id='$hiddenid'");
							
							
							$categoryskillRows = $skillObj->fetchAll($skillObj->select()
										  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
										  ->where("parent_skill_id = '$hiddenid'"));

							if (isset($categoryskillRows) && sizeof($categoryskillRows) > 0)
							{
								foreach($categoryskillRows as $idsname)
								{
									$skillObj->update($data2,"parent_skill_id='$idsname->skill_id'");
								}
							}
					}
					
					if(isset($enable) && $enable=="0")
					{
							$data3 = array("is_enabled"=>'0');
							$skillObj->update($data3,"parent_skill_id='$hiddenid'");
							
							
							$categoryskillRows = $skillObj->fetchAll($skillObj->select()
										  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
										  ->where("parent_skill_id = '$hiddenid'"));

							if (isset($categoryskillRows) && sizeof($categoryskillRows) > 0)
							{
								foreach($categoryskillRows as $idsname)
								{
									$skillObj->update($data3,"parent_skill_id='$idsname->skill_id'");
								}
							}
					}else{
							$data3 = array("is_enabled"=>'1');
							$skillObj->update($data3,"parent_skill_id='$hiddenid'");
							
							
							$categoryskillRows = $skillObj->fetchAll($skillObj->select()
										  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
										  ->where("parent_skill_id = '$hiddenid'"));

							if (isset($categoryskillRows) && sizeof($categoryskillRows) > 0)
							{
								foreach($categoryskillRows as $idsname)
								{
									$skillObj->update($data3,"parent_skill_id='$idsname->skill_id'");
								}
							}
					}
					
					$authUserNamespace->status_message = "Category has been updated successfully";
				}else{
					$skillObj->insert($data);
					$authUserNamespace->status_message = "Category has been added successfully";
				}
				//echo "<script>parent.Mediabox.close();</script>";
				echo "<script>window.parent.location='". BASEPATH ."/admin/category';</script>";
			}
		}
	}
	
public function addsubcategoryAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$this->_helper->layout()->setLayout("lightbox");
		$skillObj = new Skillzot_Model_DbTable_Skills();
		$skillrowResult = $skillObj->fetchAll($skillObj->select()
							->from(array('s'=>DATABASE_PREFIX."master_skills"),array('s.skill_name','s.skill_id'))
							->where("s.parent_skill_id='0' && s.skill_id!='0'"));
							$this->view->maincategory = $skillrowResult;
		
		$id = $this->_request->getParam("id");
		$this->view->id = $id;
		if(isset($id) && $id > 0)
		{
			$this->view->category = $skillObj->fetchRow("skill_id='$id'");
		}
		if(isset($id) && $id > 0)
		{
			$authUserNamespace->admin_page_title = "Update Sub Category";
		}else{
		    $authUserNamespace->admin_page_title = "Add Sub Category";
		}
		
		if($this->_request->isPost()){
		
			$category_name = $this->_request->getParam("subcategory_name");
			$category_id =  $this->_request->getParam("Category_name");
			$category_description = $this->_request->getParam("CategoryDescription");
			$enable = $this->_request->getParam("Enable");
			$menuable = $this->_request->getParam("menuable");
			$ordernunber = $this->_request->getParam("OrderNumber");
			$hiddenid= $this->_request->getParam("hiddenvar");
			
			
			if($this->_request->isXmlHttpRequest()){
				
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				$data = array("skill_name"=>$category_name);
			
				if(isset($id) && $id !="")
				{
					$category_exist = $skillObj->fetchAll("skill_name = '$category_name' && skill_id!='$hiddenid' && parent_skill_id!='0'");
				}else
				{
					$category_exist = $skillObj->fetchAll("skill_name = '$category_name'  && parent_skill_id!='0'");
			
				}
				if($data["skill_name"] == "")$response["data"]["subcategory_name"] = "null";
				//elseif(!preg_match("/^[a-zA-Z0-9-/ ]*$/", $data['skill_name']))$response['data']['subcategory_name'] = "invalid";
				elseif(isset($category_exist) && sizeof($category_exist) > 0)$response["data"]["subcategory_name"] = "duplicate";
				else $response["data"]["subcategory_name"] = "valid";
				
				if($category_id == "")$response["data"]["Category_name"] = "null";
				else $response["data"]["Category_name"] = "valid";
				
//				if($category_name == "")$response["data"]["subcategory_name"] = "null";
//				else $response["data"]["subcategory_name"] = "valid";
				
				if($category_description == "")$response["data"]["CategoryDescription"] = "null";
				else $response["data"]["CategoryDescription"] = "valid";
				
				if(isset($id) && $id !="")
				{
					$order_exist = $skillObj->fetchAll("skill_id!='$hiddenid' && order_id='$ordernunber' && parent_skill_id!='0' && parent_skill_id=$category_id");
				}else
				{
					$order_exist = $skillObj->fetchAll("order_id='$ordernunber' && parent_skill_id!='0'&& parent_skill_id=$category_id");
				//print_r($category_exist );exit;
				}
				
								
				if($ordernunber == "")$response["data"]["OrderNumber"] = "null";
				elseif(isset($order_exist) && sizeof($order_exist) > 0)$response["data"]["OrderNumber"] = "duplicate";
				else $response["data"]["OrderNumber"] = "valid";
				
				if($enable == "")$response["data"]["Enable"] = "null";
				else $response["data"]["Enable"] = "valid";
				
				if($menuable == "")$response["data"]["menuable"] = "null";
				else $response["data"]["menuable"] = "valid";
				
				if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";
				
				echo json_encode($response);
								
			}else{
			
				$data = array("skill_name"=>$category_name,"skill_description"=>$category_description,"parent_skill_id"=>$category_id,"skill_depth"=>'2',"is_enabled"=>$enable,"order_id"=>$ordernunber,"is_skill"=>$menuable);
				if(isset($id) && $id > 0){
					$skillObj->update($data,"skill_id='$id'");
					$authUserNamespace->status_message = "Sub Category has been updated successfully";
				}else{
					$skillObj->insert($data);
					$authUserNamespace->status_message = "Sub Category has been added successfully";
				}
				//echo "<script>parent.Mediabox.close();</script>";
				echo "<script>window.parent.location='". BASEPATH ."/admin/subcategory';</script>";
			}
		}
	}
	
	
	
public function deletedataAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$this->_helper->layout()->disableLayout();;
		$this->_helper->viewRenderer->setNoRender(true);
		$skillObj = new Skillzot_Model_DbTable_Skills();
		$category_id=$this->_request->getParam("category_id");
		$skill_id=$this->_request->getParam("skill_id");
		$subcategory_id=$this->_request->getParam("subcategory_id");
		if(isset($skill_id) && $skill_id!="")
		{
			$skillObj->delete("skill_id='$skill_id'");
			$authUserNamespace->status_message = "Skill has been deleted successfully";
			$this->_redirect('/admin/skills');
		}
		if(isset($category_id) && $category_id!="")
		{
			$SkillData= $skillObj->fetchRow("parent_skill_id='$category_id'");
			if(sizeof($SkillData) > 0)
			{
				$authUserNamespace->status_message = "Category can not be deleted";
			}else
			{
				$skillObj->delete("skill_id='$category_id'");
				$authUserNamespace->status_message = "Category has been deleted successfully";
			}
			
			$this->_redirect('/admin/category');
		}
		
		if(isset($subcategory_id) && $subcategory_id!="")
		{
			$SkillData= $skillObj->fetchRow("parent_skill_id='$subcategory_id'");
			if(sizeof($SkillData) > 0)
			{
				$authUserNamespace->status_message = "Sub category can not be deleted";
			}else{
				$skillObj->delete("skill_id='$subcategory_id'");
				$authUserNamespace->status_message = "Sub category has been deleted successfully";
			}
			$this->_redirect('/admin/subcategory');
			
		}
	}

	

	
	public function changepasswordAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		
		$authUserNamespace->admin_page_title = "Change Password";
		
		if($this->_request->isPost()) {
		
		$passwordObj = new Skillzot_Model_DbTable_Adminlogin();
		$user_id = $authUserNamespace->user_id;
		$current_password = $this->_request->getParam('CurrentPassword');
		$new_password = $this->_request->getParam('NewPassword');
		$confirm_password = $this->_request->getParam('ConfirmPassword');
		
		$response = array();
		if($this->_request->isXmlHttpRequest()){
			
			$this->_helper->layout()->disableLayout();
			$this->_helper->viewRenderer->setNoRender(true);
			
			$password_row = $passwordObj->fetchRow("admin_pwd='$current_password' and id='$user_id'");
			
			if($current_password=="")$response["data"]["CurrentPassword"] = "null";
			elseif(!sizeof($password_row) > 0)$response["data"]["CurrentPassword"] = "invalid";
			else $response["data"]["CurrentPassword"] = "valid";
			
			if($new_password == "")$response["data"]["NewPassword"] = "null";
			else $response["data"]["NewPassword"] = "valid";
			
			if($confirm_password == "")$response["data"]["ConfirmPassword"] = "null";
      		elseif($confirm_password != $new_password)$response["data"]["ConfirmPassword"] = "notmatch";
			else $response["data"]["ConfirmPassword"] = "valid";
			
			if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('notmatch',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
			else $response['returnvalue'] = "validation";
	
			echo json_encode($response);
	
			}else{
			
				$Current_pass_md5 = md5($current_password);
				$insert_new_pass = md5($new_password);
				$data['admin_pwd'] = $insert_new_pass;
				//print_r($data);exit;
				$passwordObj->update($data,"id='$user_id'");
				$authUserNamespace->status_message = "Password updated successfully.";
				$this->_redirect("/admin/home");
			}
		}
	}
	
	public function logoutAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		
		$this->_helper->layout()->disableLayout();
		$this->_helper->viewRenderer->setNoRender(true);
		
		Zend_Session::destroy(true,true);
		
		$this->_redirect('admin/index?msg=logout');
	}
	
	public function commingsoonAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$authUserNamespace->admin_page_title = "Manage CommingSoon";
		$commingsoonObj = new Skillzot_Model_DbTable_Commingsoon();
		
		$records_per_page = $this->_request->getParam('shown');
		if (isset($records_per_page) && $records_per_page!=""){
			$this->view->records_per_page = $records_per_page;
		}
		
		$searchText = addslashes($this->_request->getParam('searchtext'));
		if (isset($searchText) && $searchText!=""){
			$this->view->search_val = stripcslashes ($searchText);
		}
		
		if($records_per_page==""){
			$records_per_page = $this->_request->getParam('getPageValue');
			$this->view->records_per_page = $records_per_page;
		}
//		$searchText = "surat";
//		print $commingsoonObj->select()
//							->from(array('m'=>DATABASE_PREFIX."master_commingsoon"))
//							->where("m.city like '%".$searchText."%' || m.signin_as like '%".$searchText."%'" )
//							->order(array("order_id asc"));exit;
		if($searchText!="")
		{
			$commingsoonResult =$commingsoonObj->fetchAll($commingsoonObj->select()
							->from(array('m'=>DATABASE_PREFIX."master_commingsoon"))
							->where("m.city like '%".$searchText."%' || m.signin_as like '%".$searchText."%' || m.skill_interest like '%".$searchText."%' || m.email like '%".$searchText."%'" )
							->order(array("id asc")));
							
												
		}
		else
		{
			$commingsoonResult = $commingsoonObj->fetchAll($commingsoonObj->select()
							->from(array('m'=>DATABASE_PREFIX."master_commingsoon"))
							//->where("s.parent_skill_id!='0' && s.skill_id!='0' && skill_depth='3'")
							->order(array("id asc")));
	
		}

		if(isset($commingsoonResult) && $commingsoonResult!="")
		{
			$this->view->commingsoonResult = $commingsoonResult;
		}
			/*pagination code*/
		
		$page = $this->_request->getParam('page',1);
		//$this->view->page = $page;
		if($records_per_page=="")$records_per_page = 10;
		$record_count = sizeof($commingsoonResult);
		$paginator = Zend_Paginator::factory($commingsoonResult);
		$paginator->setItemCountPerPage($records_per_page);
		$paginator->setCurrentPageNumber($page);
		$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
		$this->view->commingsoonResult = $paginator;
		$page_number  = $record_count / 1;
		$page_number_last =  floor($page_number);
		
	}
public function deletecommingsoonAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$this->_helper->layout()->disableLayout();;
		$this->_helper->viewRenderer->setNoRender(true);
		$commingsoonObj = new Skillzot_Model_DbTable_Commingsoon();
		$id=$this->_request->getParam("id");
		
		if(isset($id) && $id!="")
		{
			
				$commingsoonObj->delete("id='$id'");
				$authUserNamespace->status_message = "Detail has been deleted successfully";
				$this->_redirect('/admin/commingsoon');
		}
		
		
	}
public function exportcommingsoonAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		
		$this->_helper->viewRenderer->setNoRender(true);
		$this->_helper->layout()->disableLayout();
		
		$commingsoonObj = new Skillzot_Model_DbTable_Commingsoon();
		$commingsoon_rows = $commingsoonObj->fetchAll('1=1','id asc');
		
		/** PHPExcel **/
		require_once dirname(dirname(__FILE__))."/models/Custom/PHPExcel/PHPExcel.php";
		
		//Create new PHPExcel object
		$objPHPExcel = new PHPExcel();
		
		//Set properties
		$objPHPExcel->getProperties()->setCreator("Pronto Infotech")
									 ->setLastModifiedBy("Pronto Infotech")
									 ->setTitle("Office 2007 XLSX Test Document")
									 ->setSubject("Office 2007 XLSX Test Document")
									 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 		 ->setKeywords("office 2007 openxml php")
							 		 ->setCategory("Test result file");
		
		//Add some data
		$i = 1;
		
		$sheet_0 = $objPHPExcel->setActiveSheetIndex(0);
		$sheet_0->setCellValue("A$i", 'Index')
				->setCellValue("B$i", 'City')
            	->setCellValue("C$i", 'Email')
            	->setCellValue("D$i", 'Signing As')
            	->setCellValue("E$i", 'Skill Interested');
//            	->setCellValue("E$i", 'Zip Code')
//            	->setCellValue("F$i", 'Mobile Number');
            	
        		    		
		$objPHPExcel->getActiveSheet()->getStyle('A1:E1')->getFont()->setBold(true);
		$i = 3;
		
		foreach ($commingsoon_rows as $t){
			
			$sheet_0->setCellValue("A$i", $t->id )
					->setCellValue("B$i", $t->city)
					->setCellValue("C$i", $t->email)
					->setCellValue("D$i", $t->signin_as)
					->setCellValue("E$i", $t->skill_interest);
//					->setCellValue("E$i", $t->zipcode)
//            		->setCellValue("F$i", $t->mobile);
			$i++;
		}
		
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);
//		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);
		
		// Rename sheet
		$objPHPExcel->getActiveSheet()->setTitle('CommingSoon Details');
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// Redirect output to a client's web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="commingsoondata.xls"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}
	public function studentsignupAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$authUserNamespace->admin_page_title = "Manage Student Signup";
		$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
		
		$records_per_page = $this->_request->getParam('shown');
		if (isset($records_per_page) && $records_per_page!=""){
			$this->view->records_per_page = $records_per_page;
		}
		
		$searchText = addslashes($this->_request->getParam('searchtext'));
		if (isset($searchText) && $searchText!=""){
			$this->view->search_val = stripcslashes ($searchText);
		}
		
		if($records_per_page==""){
			$records_per_page = $this->_request->getParam('getPageValue');
			$this->view->records_per_page = $records_per_page;
		}
		if($searchText!="")
		{
			$studentsignupResult =$studentSignupObj->fetchAll($studentSignupObj->select()
							->from(array('m'=>DATABASE_PREFIX."tx_student_tutor"))
							->where("m.first_name like '%".$searchText."%' || m.last_name like '%".$searchText."%' || m.std_email like '%".$searchText."%' ||  m.login_from like '%".$searchText."%' || m.std_city like '%".$searchText."%'  || m.std_locality like '%".$searchText."%' || m.tutor_for like '%".$searchText."%'" )
							->order(array("id desc")));
							
												
		}
		else
		{
			$studentsignupResult = $studentSignupObj->fetchAll($studentSignupObj->select()
							->from(array('m'=>DATABASE_PREFIX."tx_student_tutor"))
							//->where("s.parent_skill_id!='0' && s.skill_id!='0' && skill_depth='3'")
							->order(array("id desc")));
	
		}

		if(isset($studentsignupResult) && $studentsignupResult!="")
		{
			$this->view->studentsignupResult = $studentsignupResult;
		}
			/*pagination code*/
		
		$page = $this->_request->getParam('page',1);
		//$this->view->page = $page;
		if($records_per_page=="")$records_per_page = 10;
		$record_count = sizeof($studentsignupResult);
		$paginator = Zend_Paginator::factory($studentsignupResult);
		$paginator->setItemCountPerPage($records_per_page);
		$paginator->setCurrentPageNumber($page);
		$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
		$this->view->studentsignupResult = $paginator;
		$page_number  = $record_count / 1;
		$page_number_last =  floor($page_number);
		
	}
	public function tutorsignupAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$authUserNamespace->admin_page_title = "Manage Tutor Signup";
		$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
		
		$records_per_page = $this->_request->getParam('shown');
		if (isset($records_per_page) && $records_per_page!=""){
			$this->view->records_per_page = $records_per_page;
		}
		
		$searchText = addslashes($this->_request->getParam('searchtext'));
		if (isset($searchText) && $searchText!=""){
			$this->view->search_val = stripcslashes ($searchText);
		}
		
		if($records_per_page==""){
			$records_per_page = $this->_request->getParam('getPageValue');
			$this->view->records_per_page = $records_per_page;
		}
		
		if($searchText!="")
		{
//			$tutorsignupResult =$tutorProfile->fetchAll($tutorProfile->select()
//							->setIntegrityCheck(false)
//							->from(array('m'=>DATABASE_PREFIX."tx_tutor_profile"))
//							->joinLeft(array('a'=>DATABASE_PREFIX."master_address"),'m.tutor_add_city=a.address_id',array('a.address_value'))
//							->where("(a.address_value like '%".$searchText."%' || m.tutor_first_name like '%".$searchText."%' || m.tutor_last_name like '%".$searchText."%' || m.tutor_email like '%".$searchText."%') && m.is_active='0'" )
//							->order(array("id desc")));
							
		//---------------------------------------------------------------
		$skillobj = new Skillzot_Model_DbTable_Skills();
		$tutorProfileobj = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();	
			
			
		$skillrowResult = $skillobj->fetchRow($skillobj->select()
										   ->where("skill_name = '$searchText' && skill_id!='0'"));
	if (isset($skillrowResult) && sizeof($skillrowResult)>0)
	{
		$fetchSkill_id = $skillrowResult->skill_id;
		
		$fetchChildIdS = $skillobj->fetchAll($skillobj->select()
								  ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
								  ->where("parent_skill_id = '$fetchSkill_id' && skill_id!=0"));
		
							
		/*fetch sub categories if child exsist of main categories*/
		$final_fetchids = array();
		//echo sizeof($fetchChildIdS);exit;
		if(isset($fetchChildIdS) && sizeof($fetchChildIdS)>0){
			$j = 0;
			$final_ids = array();
			$final_ids1 = array();
			foreach($fetchChildIdS as $subids){
				$final_ids[$j] = $subids->skill_id;
				$j++;
			}
		}
		$m=0;
	//print_r($final_ids);exit;
		/*fetch skills categories  of sub categories*/
		if(isset($final_ids) && sizeof($final_ids)>0){
		for($k=0;$k<sizeof($final_ids);$k++){
			$fetchsubChildIdS = $skillobj->fetchAll($skillobj->select()
									     ->from(array('s'=>DATABASE_PREFIX.'master_skills'))
									     ->where("parent_skill_id = '$final_ids[$k]' && skill_id!=0"));
			//	print_r($fetchsubChildIdS);exit;
			if(isset($fetchsubChildIdS) && sizeof($fetchsubChildIdS)){
				foreach($fetchsubChildIdS as $skillsubids){
					$final_ids1[$m] = $skillsubids->skill_id;
					$m++;
					
				}
			}
		}
		if (sizeof($final_ids)>0 || sizeof($final_ids)>0)
		{
			$final_fetchids = array_merge($final_ids,$final_ids1); /*All id with sub cat & skill id*/
		}	
		
		}
		//print_r($final_fetchids);exit;
		
		
		
		/*Code for tutor couse table check if skill id exsist in this table and exists than fetch all tutor id and send to html*/
		$tutorCourseRows = $tutorCourseobj->fetchAll($tutorCourseobj->select()
										  ->from(array('t'=>DATABASE_PREFIX.'tx_tutor_skill_course')));
		//echo sizeof($tutorCourseRows);exit;
		$i = 0;
		$l=0;
		$final_tutor_id = array();
		//$final_fetchids[] = 12;
		foreach($tutorCourseRows as $tutorCourserow){
		
			$Skill_id = $tutorCourserow->tutor_skill_id;
			//print $Skill_id;
			$removeLastgradeId = substr($Skill_id, 0, strlen($Skill_id)-1);
			$explodedCategory = explode(",",$removeLastgradeId);
			
			if(isset($final_fetchids) && sizeof($final_fetchids)>0){
				for($g=0;$g<sizeof($final_fetchids);$g++){
					//print_r($explodedCategory);print "::";
					if(in_array($final_fetchids[$g],$explodedCategory)){
						
						//print_r($explodedCategory);exit;
						$final_tutor_id[$i] = $tutorCourserow->tutor_id;
						$i=$i+1;
					}
				}
				//print_r($final_tutor_id);exit;
			}else{
				//echo $fetchSkill_id;exit;
				if(in_array($fetchSkill_id,$explodedCategory)){
					$final_tutor_id[$l] = $tutorCourserow->tutor_id;
					$l=$l+1;
				}
			}
			//
			
		}
	
		
			$strofTutorids = implode(",",$final_tutor_id);
	}
			if (isset($final_tutor_id) && sizeof($final_tutor_id) > 0)
			{
				$condition = "id in ($strofTutorids)";
			}else{
				$condition = "(a.address_value like '%".$searchText."%' || m.tutor_first_name like '%".$searchText."%' || m.tutor_last_name like '%".$searchText."%' || m.tutor_email like '%".$searchText."%')";
			}
	//--------------------------------------------------------------------
			$tutoridsRows = $tutorProfileobj->fetchAll($tutorProfileobj->select()
																		->setIntegrityCheck(false)
																		->from(array('m'=>DATABASE_PREFIX.'tx_tutor_profile'))
																		->joinLeft(array('a'=>DATABASE_PREFIX."master_address"),'m.tutor_add_city=a.address_id',array('a.address_value'))
																		->where("($condition && is_active='0')"));
			$tutorsignupResult	= $tutoridsRows;
		}
		else
		{
			$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
							->from(array('m'=>DATABASE_PREFIX."tx_tutor_profile"))
							->where("m.is_active='0'")
							->order(array("id desc")));
	
		}
		if(isset($tutorsignupResult) && $tutorsignupResult!="")
		{
			$this->view->tutorsignupResult = $tutorsignupResult;
		}
			/*pagination code*/
		
		$page = $this->_request->getParam('page',1);
		//$this->view->page = $page;
		if($records_per_page=="")$records_per_page = 10;
		$record_count = sizeof($tutorsignupResult);
		$paginator = Zend_Paginator::factory($tutorsignupResult);
		$paginator->setItemCountPerPage($records_per_page);
		$paginator->setCurrentPageNumber($page);
		$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
		$this->view->tutorsignupResult = $paginator;
		$page_number  = $record_count / 1;
		$page_number_last =  floor($page_number);
		
	}
public function tutornewsignupAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$authUserNamespace->admin_page_title = "Manage Tutor New Signup";
		$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
		
		$records_per_page = $this->_request->getParam('shown');
		if (isset($records_per_page) && $records_per_page!=""){
			$this->view->records_per_page = $records_per_page;
		}
		
		$searchText = addslashes($this->_request->getParam('searchtext'));
		if (isset($searchText) && $searchText!=""){
			$this->view->search_val = stripcslashes ($searchText);
		}
		
		if($records_per_page==""){
			$records_per_page = $this->_request->getParam('getPageValue');
			$this->view->records_per_page = $records_per_page;
		}
		if($searchText!="")
		{
			$tutorsignupResult =$tutorProfile->fetchAll($tutorProfile->select()
							->setIntegrityCheck(false)
							->from(array('m'=>DATABASE_PREFIX."tx_tutor_profile"))
							->joinLeft(array('a'=>DATABASE_PREFIX."master_address"),'m.tutor_add_city=a.address_id',array('a.address_value'))
							->where("(a.address_value like '%".$searchText."%' || m.tutor_first_name like '%".$searchText."%' || m.tutor_last_name like '%".$searchText."%' || m.tutor_email like '%".$searchText."%') && m.is_active!='0'" )
							->order(array("id desc")));
							
												
		}
		else
		{
			$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
							->from(array('m'=>DATABASE_PREFIX."tx_tutor_profile"))
							->where("m.is_active!='0'")
							->order(array("id desc")));
	
		}
		if(isset($tutorsignupResult) && $tutorsignupResult!="")
		{
			$this->view->tutorsignupResult = $tutorsignupResult;
		}
			/*pagination code*/
		
		$page = $this->_request->getParam('page',1);
		//$this->view->page = $page;
		if($records_per_page=="")$records_per_page = 10;
		$record_count = sizeof($tutorsignupResult);
		$paginator = Zend_Paginator::factory($tutorsignupResult);
		$paginator->setItemCountPerPage($records_per_page);
		$paginator->setCurrentPageNumber($page);
		$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
		$this->view->tutorsignupResult = $paginator;
		$page_number  = $record_count / 1;
		$page_number_last =  floor($page_number);
		
	}
	public function tutordeleteAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$this->_helper->layout()->disableLayout();;
		$this->_helper->viewRenderer->setNoRender(true);
		$tutorProfileObj = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
		$travelradiousObj = new Skillzot_Model_DbTable_Travelradious();
		$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
		$id=$this->_request->getParam("id");
		$newid=$this->_request->getParam("newid");
		
		
		if(isset($id) && $id!="")
		{
			
				$tutorProfileObj->delete("id='$id'");
				$tutorCourseobj->delete("tutor_id='$id'");
				$travelradiousObj->delete("tutor_id='$id'");
				$branchdetailObj->delete("tutor_id='$id'");
				$authUserNamespace->status_message = "Detail has been deleted successfully";
				$this->_redirect('/admin/tutorsignup');
		}
		if(isset($newid) && $newid!="")
		{
			
				$tutorProfileObj->delete("id='$newid'");
				$tutorCourseobj->delete("tutor_id='$newid'");
				$travelradiousObj->delete("tutor_id='$newid'");
				$branchdetailObj->delete("tutor_id='$newid'");
				$authUserNamespace->status_message = "Detail has been deleted successfully";
				$this->_redirect('/admin/tutornewsignup');
		}
		
		
	}
public function studentdeleteAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$this->_helper->layout()->disableLayout();;
		$this->_helper->viewRenderer->setNoRender(true);
		$studentSignupObj = new Skillzot_Model_DbTable_Studentsignup();
		$id=$this->_request->getParam("id");
		
		if(isset($id) && $id!="")
		{
			
				$studentSignupObj->delete("id='$id'");
				$authUserNamespace->status_message = "Detail has been deleted successfully";
				$this->_redirect('/admin/studentsignup');
		}
		
		
	}
public function editstatusAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$this->_helper->layout()->setLayout("lightbox");
		
		$id = $this->_request->getParam("id");
		$this->view->id = $id;
		
		$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
		
		if(isset($id) && $id > 0)
		{
			$this->view->category = $tutorProfile->fetchRow("id='$id'");
		}
		
		if(isset($id) && $id > 0)
		{
			$authUserNamespace->admin_page_title = "Update Status";
		}
		
		if($this->_request->isPost()){
		
			$status_value = $this->_request->getParam("statusflag");
			
			if($this->_request->isXmlHttpRequest()){
				
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				
				
				if($status_value == "")$response["data"]["statusflag"] = "onlyselectnull";
				else $response["data"]["statusflag"] = "valid";
				
				if(!in_array('null',$response['data']) && !in_array('onlyselectnull',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";
				
				echo json_encode($response);
				
			}else{
				if ($status_value=="0"){
					$data = array("is_active"=>"1");
					$tutorProfile->update($data,"id='$id'");
					echo "<script>window.parent.location='". BASEPATH ."/admin/tutorsignup';</script>";
				}else{
					$data = array("is_active"=>"0");
					$tutorProfile->update($data,"id='$id'");
					echo "<script>window.parent.location='". BASEPATH ."/admin/tutornewsignup';</script>";
				}
				//echo "<script>parent.Mediabox.close();</script>";
				
			}
		}
	}
	public function getordernumberAction()
	{
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$this->_helper->layout()->disableLayout();
		$skillObj = new Skillzot_Model_DbTable_Skills();
		
			if($this->_request->isPost()){
			
				if($this->_request->isXmlHttpRequest()){
				$category_id = $this->_request->getParam("category_id");
				$subcat_id = $this->_request->getParam("subcategory_id");
				
				if (isset($subcat_id) && $subcat_id!="")
				{
					//echo "in";
					$temp_id =  $subcat_id;
				}else{
					$temp_id =  $category_id;
				}
				//echo "---temp--".$temp_id;exit;
				$category_rows = $skillObj->fetchAll($skillObj->select()
															  ->from(array('s'=>DATABASE_PREFIX."master_skills"),array('s.order_id'))
															  ->where("s.parent_skill_id=$temp_id")
															  ->order(array("order_id desc")));
															//  print_r($category_rows);
				
				$tags = "";
				$tags['orderid'] = $category_rows[0]['order_id'] + 1;
				
				echo json_encode($tags);exit;
				}
	  		}
	}
public function reviewAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$authUserNamespace->admin_page_title = "Review Display";
		$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
		$tutorreviewobj = new Skillzot_Model_DbTable_Review();
		
		$records_per_page = $this->_request->getParam('shown');
		if (isset($records_per_page) && $records_per_page!=""){
			$this->view->records_per_page = $records_per_page;
		}
		
		$searchText = addslashes($this->_request->getParam('searchtext'));
		if (isset($searchText) && $searchText!=""){
			$this->view->search_val = stripcslashes ($searchText);
		}
		
		if($records_per_page==""){
			$records_per_page = $this->_request->getParam('getPageValue');
			$this->view->records_per_page = $records_per_page;
		}
		if($searchText!="")
		{
			$tutorreviewResult =$tutorreviewobj->fetchAll($tutorreviewobj->select()
							->setIntegrityCheck(false)
							->from(array('m'=>DATABASE_PREFIX."tutor_review"))
							->joinLeft(array('a'=>DATABASE_PREFIX."tx_student_tutor"),'m.tutor_id=a.id',array('a.first_name','a.last_name','a.std_email','a.std_city','a.std_locality','a.std_phone','a.std_pincode'))
							->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'m.tutor_id=t.id',array('t.tutor_first_name','t.tutor_last_name','t.tutor_email'))
							->where("(t.tutor_first_name like '%".$searchText."%' || t.tutor_last_name like '%".$searchText."%' || t.tutor_email like '%".$searchText."%' || a.first_name like '%".$searchText."%' || a.last_name like '%".$searchText."%' || a.std_email like '%".$searchText."%') " )
							->order(array("id desc")));
												
		}
		else
		{
			$tutorreviewResult = $tutorreviewobj->fetchAll($tutorreviewobj->select()
							->setIntegrityCheck(false)
							->from(array('m'=>DATABASE_PREFIX."tutor_review"))
							->joinLeft(array('a'=>DATABASE_PREFIX."tx_student_tutor"),'m.tutor_id=a.id',array('a.first_name','a.last_name','a.std_email','a.std_city','a.std_locality','a.std_phone','a.std_pincode'))
							->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'m.tutor_id=t.id',array('t.tutor_first_name','t.tutor_last_name','t.tutor_email'))
							->order(array("m.id desc")));
	
		}
		if(isset($tutorreviewResult) && $tutorreviewResult!="")
		{
			$this->view->tutorreviewResult = $tutorreviewResult;
		}
			/*pagination code*/
		
		$page = $this->_request->getParam('page',1);
		//$this->view->page = $page;
		if($records_per_page=="")$records_per_page = 10;
		$record_count = sizeof($tutorreviewResult);
		$paginator = Zend_Paginator::factory($tutorreviewResult);
		$paginator->setItemCountPerPage($records_per_page);
		$paginator->setCurrentPageNumber($page);
		$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
		$this->view->tutorreviewResult = $paginator;
		$page_number  = $record_count / 1;
		$page_number_last =  floor($page_number);
	}
	
	public function messagedetailAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$authUserNamespace->admin_page_title = "View Messages";
		$mailMessagesObj = new Skillzot_Model_DbTable_Mailmessages();
		$tutorreviewobj = new Skillzot_Model_DbTable_Review();
		
		$records_per_page = $this->_request->getParam('shown');
		if (isset($records_per_page) && $records_per_page!=""){
			$this->view->records_per_page = $records_per_page;
		}
		
		$searchText = addslashes($this->_request->getParam('searchtext'));
		if (isset($searchText) && $searchText!=""){
			$this->view->search_val = stripcslashes ($searchText);
		}
		
		if($records_per_page==""){
			$records_per_page = $this->_request->getParam('getPageValue');
			$this->view->records_per_page = $records_per_page;
		}
		if($searchText!="")
		{
			$messageResult =$mailMessagesObj->fetchAll($mailMessagesObj->select()
							->setIntegrityCheck(false)
							->from(array('m'=>DATABASE_PREFIX."message"))
							->joinLeft(array('a'=>DATABASE_PREFIX."tx_student_tutor"),'m.tutor_id=a.id',array('a.first_name','a.last_name','a.std_email','a.std_city','a.std_locality','a.std_phone','a.std_pincode'))
//							->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'m.tutor_id=t.id',array('t.tutor_first_name','t.tutor_last_name','t.tutor_email'))
							->where("(t.tutor_first_name like '%".$searchText."%' || t.tutor_last_name like '%".$searchText."%' || t.tutor_email like '%".$searchText."%' || a.first_name like '%".$searchText."%' || a.last_name like '%".$searchText."%' || a.std_email like '%".$searchText."%') " )
							->order(array("id desc")));
												
		}
		else
		{
			$messageResult = $mailMessagesObj->fetchAll($mailMessagesObj->select()
							->setIntegrityCheck(false)
							->from(array('m'=>DATABASE_PREFIX."message"))
							//->joinLeft(array('a'=>DATABASE_PREFIX."tx_student_tutor"),'m.tutor_id=a.id',array('a.first_name','a.last_name','a.std_email','a.std_city','a.std_locality','a.std_phone','a.std_pincode'))
							//->joinLeft(array('t'=>DATABASE_PREFIX."tx_tutor_profile"),'m.tutor_id=t.id',array('t.tutor_first_name','t.tutor_last_name','t.tutor_email'))
							->where("sent_delete_flag='0'")
							->order(array("m.id desc")));
	
		}
		if(isset($messageResult) && $messageResult!="")
		{
			$this->view->messageResult = $messageResult;
		}
			/*pagination code*/
		
		$page = $this->_request->getParam('page',1);
		//$this->view->page = $page;
		if($records_per_page=="")$records_per_page = 10;
		$record_count = sizeof($messageResult);
		$paginator = Zend_Paginator::factory($messageResult);
		$paginator->setItemCountPerPage($records_per_page);
		$paginator->setCurrentPageNumber($page);
		$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
		$this->view->messageResult = $paginator;
		$page_number  = $record_count / 1;
		$page_number_last =  floor($page_number);
		
	}
	
		public function travelradiousAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$authUserNamespace->admin_page_title = "Travel radious";
		$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
		$travelradiousObj = new Skillzot_Model_DbTable_Travelradious();
		
		$records_per_page = $this->_request->getParam('shown');
		if (isset($records_per_page) && $records_per_page!=""){
			$this->view->records_per_page = $records_per_page;
		}
		
		$searchText = addslashes($this->_request->getParam('searchtext'));
		if (isset($searchText) && $searchText!=""){
			$this->view->search_val = stripcslashes ($searchText);
		}
		
		if($records_per_page==""){
			$records_per_page = $this->_request->getParam('getPageValue');
			$this->view->records_per_page = $records_per_page;
		}
		if($searchText!="")
		{
			$tutorsignupResult =$travelradiousObj->fetchAll($travelradiousObj->select()
							->setIntegrityCheck(false)
							->from(array('m'=>DATABASE_PREFIX."travel_radious"))
							->joinLeft(array('a'=>DATABASE_PREFIX."tx_tutor_profile"),'m.tutor_id=a.id',array('a.tutor_first_name','a.tutor_last_name'))
							->where("(a.tutor_first_name like '%".$searchText."%' || a.tutor_last_name like '%".$searchText."%')" )
							->order(array("lastupdatedate desc")));
												
		}
		else
		{
			$tutorsignupResult = $travelradiousObj->fetchAll($travelradiousObj->select()
							->from(array('m'=>DATABASE_PREFIX."travel_radious"))
							//->where("m.suburbs_id!='0'")
							->order(array("lastupdatedate desc")));
	
		}
		if(isset($tutorsignupResult) && $tutorsignupResult!="")
		{
			$this->view->tutorsignupResult = $tutorsignupResult;
		}
			/*pagination code*/
		//echo "in";exit;
		$page = $this->_request->getParam('page',1);
		//$this->view->page = $page;
		if($records_per_page=="")$records_per_page = 10;
		$record_count = sizeof($tutorsignupResult);
		$paginator = Zend_Paginator::factory($tutorsignupResult);
		$paginator->setItemCountPerPage($records_per_page);
		$paginator->setCurrentPageNumber($page);
		$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
		$this->view->tutorsignupResult = $paginator;
		$page_number  = $record_count / 1;
		$page_number_last =  floor($page_number);
		
	}
public function edittravelradiousAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$this->_helper->layout()->setLayout("lightbox");
		
		$id = $this->_request->getParam("id");
		$this->view->id = $id;
		
		$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
		$travelradiousObj = new Skillzot_Model_DbTable_Travelradious();
		if(isset($id) && $id > 0)
		{
			$this->view->category = $tutorProfile->fetchRow("id='$id'");
		}
		$localitysuburbsObj = new Skillzot_Model_DbTable_Localitysuburbs();
		$suburbsData = $localitysuburbsObj->fetchAll($localitysuburbsObj->select()
															  ->from(array('s'=>DATABASE_PREFIX."locality_suburbs"))
															  //->where("s.is_active='1'")
															  ->order(array("s.suburbs_name asc")));
		if (isset($suburbsData) && sizeof($suburbsData)>0)
		{
			$this->view->suburbsdata = $suburbsData;
		}	
		
		if($this->_request->isPost()){
		
			$suburbsID = $this->_request->getParam("multile_sub_id");
			
			if($this->_request->isXmlHttpRequest()){
				
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				
				
				if($suburbsID=="" )$response["data"]["suburbs_name"] = "selectatleast";
				else $response["data"]["suburbs_name"] = "valid";
//				
				if(!in_array('selectatleast',$response['data']) && !in_array('null',$response['data']) && !in_array('onlyselectnull',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";
				
				echo json_encode($response);
				
			}else{
					$suburbsID = substr($suburbsID, 1);
					$os = explode(",",$suburbsID);
					if (in_array("1", $os) || in_array("5", $os) || in_array("9", $os)) 
					{
   						 $suburbsID=$suburbsID.","."13";
					}
					if(in_array("7", $os) || in_array("8", $os))
					{
						 $suburbsID=$suburbsID.","."14";
					}		
					$data = array("suburbs_id"=>$suburbsID);
					$travelradiousObj->update($data,"id='$id'");
					echo "<script>window.parent.location='". BASEPATH ."/admin/travelradious';</script>";
				//echo "<script>parent.Mediabox.close();</script>";
				
			}
		}
	}
	
public function skillfriendlyAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->setLayout("admin");
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$authUserNamespace->admin_page_title = "Manage Skill";
		$skillObj = new Skillzot_Model_DbTable_Skills();
		$skilldisplayObj = new Skillzot_Model_DbTable_Skillslisting();
		
		$records_per_page = $this->_request->getParam('shown');
		if (isset($records_per_page) && $records_per_page!=""){
			$this->view->records_per_page = $records_per_page;
		}
		
		$searchText = addslashes($this->_request->getParam('searchtext'));
		if (isset($searchText) && $searchText!=""){
			$this->view->search_val = stripcslashes ($searchText);
		}
		
		if($records_per_page==""){
			$records_per_page = $this->_request->getParam('getPageValue');
			$this->view->records_per_page = $records_per_page;
		}
		
		if($searchText!="")
		{
			$skillrowResult =$skilldisplayObj->fetchAll($skilldisplayObj->select()
							->from(array('s'=>DATABASE_PREFIX."master_skills_for_diplay_listing"))
							->where("s.skill_id!='0' && s.skill_depth!='0' && (s.skill_name like '%".$searchText."%' || s.skill_uniq like '%".$searchText."%' || s.page_title like '%".$searchText."%')")
							->order(array("s.skill_depth asc")));
												
		}
		else
		{
			$skillrowResult = $skilldisplayObj->fetchAll($skilldisplayObj->select()
							->from(array('s'=>DATABASE_PREFIX."master_skills_for_diplay_listing"))
							->where("s.skill_id!='0' && skill_depth!='0'")
							->order(array("s.skill_depth asc")));
	
		}

		if(isset($skillrowResult) && $skillrowResult!="")
		{
			$this->view->skillrowResult = $skillrowResult;
		}
			/*pagination code*/
		
		$page = $this->_request->getParam('page',1);
		//$this->view->page = $page;
		if($records_per_page=="")$records_per_page = 20;
		$record_count = sizeof($skillrowResult);
		$paginator = Zend_Paginator::factory($skillrowResult);
		$paginator->setItemCountPerPage($records_per_page);
		$paginator->setCurrentPageNumber($page);
		$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
		$this->view->skillrowResult = $paginator;
		$page_number  = $record_count / 1;
		$page_number_last =  floor($page_number);
												   
	}
	public function editfridlyskillAction(){
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$this->_helper->layout()->setLayout("lightbox");
		$skillObj = new Skillzot_Model_DbTable_Skills();
		$skilldisplayObj = new Skillzot_Model_DbTable_Skillslisting();
		
		$skillrowResult = $skilldisplayObj->fetchAll($skilldisplayObj->select()
							->from(array('s'=>DATABASE_PREFIX."master_skills_for_diplay_listing"),array('s.skill_name','s.skill_id'))
							->where("s.parent_skill_id='0' && s.skill_id!='0'"));
		$this->view->maincategory = $skillrowResult;
		$id = $this->_request->getParam("id");
		$this->view->id = $id;
		
		//echo $id;exit;
		if(isset($id) && $id > 0)
		{
			$fetch_data = $skilldisplayObj->fetchRow("skill_id='$id'");
			$this->view->category =$fetch_data ;
		}

		if(isset($id) && $id > 0)
		{
			$authUserNamespace->admin_page_title = "Update Skill";
		}else{
		    $authUserNamespace->admin_page_title = "Add Skill";
		}

		if($this->_request->isPost()){

			$category_id = $this->_request->getParam("Category_name");
			$subcategory_name = $this->_request->getParam("categories_sub");
			$skill_name = $this->_request->getParam("Skill_name");
			$skill_description = $this->_request->getParam("SkillDescription");
			$enable = $this->_request->getParam("Enable");
			$menuable = $this->_request->getParam("menuable");
			$ordernunber = $this->_request->getParam("OrderNumber");
			$hiddenid= $this->_request->getParam("hiddenvar");
			
			$urlName= $this->_request->getParam("urlName");
			$pageTitle= $this->_request->getParam("skillPagetitle");


			if($this->_request->isXmlHttpRequest()){

				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();

				if($skill_description == "")$response["data"]["SkillDescription"] = "null";
				else $response["data"]["SkillDescription"] = "valid";
				
				if($urlName == "")$response["data"]["urlName"] = "null";
				else $response["data"]["urlName"] = "valid";
				
				if($pageTitle == "")$response["data"]["skillPagetitle"] = "null";
				else $response["data"]["skillPagetitle"] = "valid";

				if(!in_array('null',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";

				echo json_encode($response);

			}else{
					$data = array("skill_description"=>$skill_description,"skill_uniq"=>$urlName,"page_title"=>$pageTitle);
					
					$skilldisplayObj->update($data,"skill_id='$id'");
					$authUserNamespace->status_message = "Skills has been updated successfully";
				//echo "<script>parent.Mediabox.close();</script>";
				echo "<script>window.parent.location='". BASEPATH ."/admin/skillfriendly';</script>";
			}
		}
	}
public function tutorfriendlyurlAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$authUserNamespace->admin_page_title = "Manage Tutor Friendly Url";
		$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
		
		$records_per_page = $this->_request->getParam('shown');
		if (isset($records_per_page) && $records_per_page!=""){
			$this->view->records_per_page = $records_per_page;
		}
		
		$searchText = addslashes($this->_request->getParam('searchtext'));
		if (isset($searchText) && $searchText!=""){
			$this->view->search_val = stripcslashes ($searchText);
		}
		
		if($records_per_page==""){
			$records_per_page = $this->_request->getParam('getPageValue');
			$this->view->records_per_page = $records_per_page;
		}
		
		if($searchText!="")
		{
			$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
							->setIntegrityCheck(false)
							->from(array('m'=>DATABASE_PREFIX.'tx_tutor_profile'),array('m.id','m.tutor_first_name','m.tutor_last_name','m.company_name','m.tutor_url_name','m.userid'))
							->where("(m.tutor_first_name like '%".$searchText."%' || m.tutor_last_name like '%".$searchText."%' || m.company_name like '%".$searchText."%' || m.tutor_url_name like '%".$searchText."%') && m.is_active='0'" ));
		}
		else
		{
			$tutorsignupResult = $tutorProfile->fetchAll($tutorProfile->select()
							->from(array('m'=>DATABASE_PREFIX."tx_tutor_profile"),array('m.id','m.tutor_first_name','m.tutor_last_name','m.company_name','m.tutor_url_name','m.userid'))
							->where("m.is_active='0'")
							->order(array("id desc")));
	
		}
		//echo $searchText;exit;
		if(isset($tutorsignupResult) && sizeof($tutorsignupResult)>0)
		{
			$this->view->tutorsignupResult = $tutorsignupResult;
		}
			/*pagination code*/
		
		$page = $this->_request->getParam('page',1);
		//$this->view->page = $page;
		if($records_per_page=="")$records_per_page = 10;
		$record_count = sizeof($tutorsignupResult);
		$paginator = Zend_Paginator::factory($tutorsignupResult);
		$paginator->setItemCountPerPage($records_per_page);
		$paginator->setCurrentPageNumber($page);
		$this->view->pagination_config = array('total_items'=>$record_count,'items_per_page'=>$records_per_page,'style'=>'digg');
		$this->view->tutorsignupResult = $paginator;
		$page_number  = $record_count / 1;
		$page_number_last =  floor($page_number);
		
	}
public function edittutorfriendlyurlAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id=="")$this->_redirect("/admin");
		$this->_helper->layout()->setLayout("lightbox");
		
		$id = $this->_request->getParam("id");
		$this->view->id = $id;
		$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
		
		if(isset($id) && $id > 0)
		{
			$this->view->category = $tutorProfile->fetchRow("id='$id'");
		}
		
		if(isset($id) && $id > 0)
		{
			$authUserNamespace->admin_page_title = "Update Status";
		}
		
		if($this->_request->isPost()){
		
			$status_value = $this->_request->getParam("tutorurlnamefriendly");
			$usernameValue = $this->_request->getParam("tutorusername");
			if($this->_request->isXmlHttpRequest()){
				
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();
				
				
				if($status_value == "")$response["data"]["tutorurlnamefriendly"] = "null";
				else $response["data"]["tutorurlnamefriendly"] = "valid";
				
				if($usernameValue == "")$response["data"]["tutorusername"] = "null";
				else $response["data"]["tutorusername"] = "valid";
				
				if(!in_array('null',$response['data']) && !in_array('onlyselectnull',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate',$response['data']))$response['returnvalue'] = "success";
				else $response['returnvalue'] = "validation";
				
				echo json_encode($response);
				
			}else{
					$data = array("tutor_url_name"=>$status_value,"userid"=>$usernameValue);
					$tutorProfile->update($data,"id='$id'");
					echo "<script>window.parent.location='". BASEPATH ."/admin/tutorfriendlyurl';</script>";
				
			}
		}
	}
	public function testtestAction(){
	
	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$this->_helper->layout()->disableLayout();
	$this->_helper->viewRenderer->setNoRender(true);
	
	$tutorProfile = new Skillzot_Model_DbTable_Tutorprofile();
	$travelradiousObj = new Skillzot_Model_DbTable_Travelradious();
	$companyaddressObj = new Skillzot_Model_DbTable_Companyaddresslist();
	$branchdetailObj = new Skillzot_Model_DbTable_Branchdetails();
	$tutorMessageobj = new Skillzot_Model_DbTable_Messagetutor();
	
	//$radiousData = $travelradiousObj->fetchAll();
	
		
		$fetchallturoriddata = $tutorProfile->fetchAll($tutorProfile->select()
										->from(array('p'=>DATABASE_PREFIX."tx_tutor_profile"),array('id','company_name','tutor_first_name','tutor_last_name','userid'))
										->order(array("p.id asc")));
										echo sizeof(fetchallturoriddata);
										foreach ($fetchallturoriddata as $tutorrow)
										{
											$userName = $tutorrow->tutor_first_name.$tutorrow->tutor_last_name;											
											$userNametopass= preg_replace("/[^A-Za-z0-9]/", "",$userName);
											echo "RedirectMatch 301". " "."http://www.skillzot.com/".strtolower($tutorrow->userid)." "."http://www.skillzot.com/".$tutorrow->userid;
											echo "<br/>";
										}	
	
					
	 exit;
		foreach($radiousData as $tt)
			{
			
			$profile_rows1 = $tutorProfile->fetchRow($tutorProfile->select()
											->setIntegrityCheck(false)
											->from(array('s'=>DATABASE_PREFIX."tx_tutor_profile"),array('s.id'))
											->where("s.id='$tt->tutor_id'"));
			
			if(isset($profile_rows1) && sizeof($profile_rows1)>0)
			{
				//echo "dd";
			}else{
			//$tutorMessageobj->delete("to_id='$tt->tutor_id'");
				//echo $tt->tutor_id;
			}
			echo "<br/>";
			}
	//print_r($profile_rows1);exit;										
	
	}
}