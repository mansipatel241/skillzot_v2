<?php
class ProgressbarController extends Zend_Controller_Action{

	public function init(){
	$this->_helper->layout->setLayout('layout');
	
	}
	public function fileuploadAction(){
		
		
		$authUserNamespace = new Zend_Session_Namespace('Eccella_Auth');
//		if(!isset($authUserNamespace->user_id) || $authUserNamespace->user_id==""){
//			include_once "ErrorTemplates/access_denied.php";
//			exit;
//		}
		$picsdetailObj = new Eccella_Model_DbTable_Picsdetail();
		//$check_access = new Realestate_Model_Custom_Acl();
		//$check_access->checkAccess($this->getRequest()->getModuleName(),$this->getRequest()->getControllerName(),$this->getRequest()->getActionName(),"file");
		
		//ini_set("memory_limit", "400M");
		ini_set("memory_limit", "20M");
		set_time_limit(0);
		
		$path = implode("/",explode("|",$path));
		$this->view->user_id_info = "1";
		//echo $path;exit;
		$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
		if($this->_request->isPost()){
		$path="docs";
		//echo $path;exit;
			if(isset($path) && trim($path)!=""){
				//echo "enter";exit;
				// disable layout and view rendering for validation
				
				$json = array();
				$file_cnt = 1;
				
					$file_size=$_FILES['uploadfromcourse']['size'];
					//$authUserNamespace->filesize = $file_size;
					
					$file_name=$_FILES['uploadfromcourse']['name'];
					$file_type=$_FILES['uploadfromcourse']['type'];
					$upload_path = $_FILES['uploadfromcourse']['tmp_name'];

					//$target_path2 = "picfolder/images/".$_FILES['filetoupload0']['name'];
					//move_uploaded_file($_FILES['filetoupload0']['tmp_name'],$target_path2 );
					
					
				$pict = "";
				$pic = $_FILES['uploadfromcourse'];	
				$i = 0;
				
				//$sizeimage = sizeof($image);
				$cntimage=0;
				foreach ($image as $r)
				{
				
					//echo "<script>alert(.$r['type'].);</script>";
					//echo "<script>alert('in1');</script>";
					if (( $r['type'] == "image/gif" || $r['type'] == "image/jpeg" || $r['type'] == "image/jpg" ||  $r['type'] == "image/pjpeg"))
					{
						if($r['size']<=1048576){
						
							//echo "<script>alert('in');</script>";
						$userid = "1";
						$img_type=$r['type'];
						
						$temp=$r['tmp_name'];
						//echo "<script>alert($temp);</script>";
						$fd=fopen($temp,"rb");
						$img_content=fread($fd,$r['size']);
						
							$jpeg_quality = 90;
							$ThumbWidth =160;
							$src = file_get_contents($r['tmp_name']);
		    				$img_r = imagecreatefromstring($src);
		    				list($width, $height) = getimagesize($r['tmp_name']);
							//calculate the image ratio
							$imgratio=$width/$height;
							if ($imgratio>1){
							$newwidth = $ThumbWidth;
							$newheight = $ThumbWidth/$imgratio;
							}else{
							$newheight = $ThumbWidth;
							$newwidth = $ThumbWidth*$imgratio;
							}
							// create a new temporary image
     						$tmp_img = imagecreatetruecolor( $newwidth, $newheight );
     						
     				
     						
							//the resizing is going on here!
							
							imagecopyresized($tmp_img, $img_r, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
							$target_path1 = "docs/Thumb_".$r["name"];
	    					imagejpeg($tmp_img,$target_path1,$jpeg_quality );
	    					$target_path2 = "docs/".$r["name"];
							move_uploaded_file($r["name"],$target_path1);
							$filecheck = basename($target_path1);
		  					$ext = substr($filecheck, strrpos($filecheck, '.') + 1);
							$ext = strtolower($ext);
							if($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "pjpeg" || $ext == "png"){
						   		$thumb_type="image/".$ext;
						   	
							}
							$imagethumbcontent=file_get_contents($target_path1);
							//$target_path2 = "picfolder/".$r["name"]; 
							//echo $target_path2 ;exit;
						
							$hostName = $_SERVER['HTTP_HOST'];
							
							$target_path4 = $hostName.BASEPATH."/".$target_path2;
			
							move_uploaded_file($r["tmp_name"],$target_path2 );
							//move_uploaded_file($r["tmp_name"],$target_path1 );
							$data=array("user_id"=>$userid,"img_path"=>$target_path4,"img_type"=>$img_type,"img_content"=>$img_content,"img_thumb_type"=>$thumb_type,"img_thumb_content"=>$imagethumbcontent);
							//print_r($data);exit;
							$picsdetailObj->insert($data);
							
							$cntimage++;
							//echo "<script>alert($cntimage);</script>";
							$sizeimage = sizeof($image);
							//echo "<script>alert($sizeimage);</script>";
				
							if($cntimage == sizeof($image)){
								//echo "<script>alert('in');</script>";
								$json="success";
								echo json_encode($json);
								//Mediabox.close();exit;
							}
						}else{
						
							$json['message']="notsuccess";
							echo json_encode($json);
							echo "<script>alert('File size is greater than 1MB.Please upload again.');</script>";
							echo "<script>window.parent.location.reload();</script>";
						}
					}
					else
					{
						echo "notin";
						//$authUserNamespace->errormsg="Please select only image file";
						//$this->_redirect('/index/addpics');
					}
				}
			}
			else{
				$json['message'] = "Please Provide Path To Upload.";
				echo json_encode($json);
			}
			
			
		}
		
	}
	

     public function progressAction() {

        // check if a request is an AJAX request
        if (!$this->getRequest()->isXmlHttpRequest()) {
            throw new Zend_Controller_Request_Exception('Not an AJAX request detected');
        }

        $uploadId = $this->getRequest()->getParam('id');

        // this is the function that actually reads the status of uploading
        $data = uploadprogress_get_info($uploadId);

        $bytesTotal = $bytesUploaded = 0;

        if (null !== $data) {
			
            $bytesTotal = $data['bytes_total'];
            $bytesUploaded = $data['bytes_uploaded'];
        }
		//print "total byte:::::".$bytesTotal;
		/*if($bytesTotal >= 1024 && $bytesTotal <= 1048576)
		{
			//print "total byte if   ::::::::::::::".$bytesTotal;
			 $bytesTotal =  $bytesTotal * 100;
			 $bytesUploaded = $bytesUploaded * 100;
			// print "total byte if   ::::::::::::::".$bytesTotal;
			// print "uploaded if::::::::".$bytesUploaded; 
			// print "total byte after if   ::::::::::::::".$bytesTotal;
		}*/
		$bytesTotal = $bytesTotal / 1024;
        if($bytesTotal / 1024 < 1024) 
            {
				$bytesTotal = number_format($bytesTotal / 1024, 0);
            } 
			
			$bytesUploaded = $bytesUploaded / 1024;
		 if($bytesUploaded / 1024 < 1024) 
         {
				$bytesUploaded = number_format($bytesUploaded / 1024, 2);
         } 
		// print "uploaded::::".$bytesUploaded; 
		$bytesUploaded=ceil($bytesUploaded);
			//print "uploaded after set::::".$bytesUploaded; 
       // print "total byte new::::::".$bytesTotal."<br/>";
		//print "uploaded::::".$bytesUploaded; 
		
        $adapter = new Zend_ProgressBar_Adapter_JsPull();
        $progressBar = new Zend_ProgressBar($adapter, 0, $bytesTotal, 'uploadProgress');
		if($bytesTotal > 0 && $bytesUploaded > 0)
		{
			//print "in if check ::::::bytestotal:;;". $bytesTotal;
			//print "in if check ::::::bytesupload:;;".$bytesUploaded; 
			if ($bytesTotal == $bytesUploaded) 
			{
				//print "if ::::::: ".$bytesUploaded;
				$progressBar->finish();
			
			} 
			else 
			{
				//print "if else:::::: ".$bytesUploaded;
				$progressBar->update($bytesUploaded);
			}
		}
		
		else
		{
			//print "else::::::".$bytesUploaded;
			$progressBar->update($bytesUploaded);
		}
    }
}
?>