<?php
class UploadimagenewController extends Zend_Controller_Action
{
	public function init(){

	$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
	$id = $this->_request->getParam("id");
	$this->view->id = $id;
	$tutorId = $id;
	if (isset($tutorId)) {
		//echo "in";
		$authUserNamespace->admintutorid=$tutorId;
		$authUserNamespace->logintype = '1';
		}
    		
	}
	public function indexAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
	 	$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
	 	$img_tutor_id = $this->_request->getParam("id");
	 	
		if($this->_request->isPost())
		{
			$uploadfromcourse = $this->_request->getParam("uploadfromcourse");
			
			//echo 'hi'.$addfromcomp;
			if($this->_request->isXmlHttpRequest())
			{
				$this->_helper->layout()->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);
				$response = array();

				$filecheck1 = basename($uploadfromcourse);
  				$ext = substr($filecheck1, strrpos($filecheck1, '.') + 1);
				$ext = strtolower($ext);
				
				if($uploadfromcourse == "")$response["data"]["uploadfromcourse"] = "imageuploaderror";
				elseif($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "pjpeg" || $ext == "png")$response['data']['uploadfromcourse']="valid";
				else $response["data"]["uploadfromcourse"] = "invalid";
			
				if(!in_array('null',$response['data'])&& !in_array('imageuploaderror',$response['data']) && !in_array('invalid',$response['data']) && !in_array('duplicate_combination',$response['data'])&& !in_array('duplicate',$response['data']))
				{
						$response['returnvalue'] = "success";
				}
				else
				{
					$response['returnvalue'] = "validation";
				}
				echo json_encode($response);
			}
			else
			{
				 $uploadfromtype = $_FILES["uploadfromcourse"]["type"];
				 $tmpName1 = $_FILES["uploadfromcourse"]["tmp_name"];
				 $fileName = $_FILES["uploadfromcourse"]["name"];
				 $image_size = $_FILES["uploadfromcourse"]["size"];
				if(isset($image_size) && $image_size > 5242880 ){
						$authUserNamespace->courseimageerror = "Image size is grater than 5 Mb";
						$this->_redirect('/uploadimagenew/index');
//						if (isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
//						{
//							
//							$defaultcourseId = $authUserNamespace->defultcourseid;
//							define('DEFAULTCOURSEID',$defaultcourseId);
//							//print DEFAULTCOURSEID;
//							//exit;
//							echo "<script>window.parent.location='". BASEPATH ."/editprofile/editcourse/defaultcorseid/". DEFAULTCOURSEID ."';</script>";
//						
//						}else{
//							echo "<script>window.parent.location='". BASEPATH ."/tutor/coursedetail/#jerrorimg';</script>";
//						}
					}else{	
				 $fp1 = fopen($tmpName1, 'r');
				 $addfromcomptypecontent = fread($fp1, filesize($tmpName1));
				 $content = fread($fp1, filesize($tmpName1));
				 fclose($fp1);
				
				$this->view->image_type=$uploadfromtype;
				$this->view->tmpName=$tmpName1;
				
					$fileInfo = $_FILES["uploadfromcourse"];
				
					list($imagewidth, $imageheight, $type, $attr) = getimagesize($tmpName1);
					
					if((isset($imagewidth) && $imagewidth < 450) ||(isset($imageheight) && $imageheight < 179)){
						$authUserNamespace->coursewidtherror = "Image size less than 550 * 179 pixels.";
						$this->_redirect('/uploadimagenew/index');
					}else{
					list($newimagewidth, $newimageheight, $newtype, $newattr) = getimagesize($tmpName1);

					if($imagewidth > 700){
						$propimgw = 700/$imagewidth;
						$imagewidth = 700;
						$imageheight = $imageheight*$propimgw;
					}
		
					if($imageheight > 420){
						$propimgh = 420/$imageheight;
						$imageheight = 420;
						$imagewidth = $imagewidth*$propimgh;
					}
					$jpeg_quality = 90;
					$src = file_get_contents($tmpName1);
			    	$img_r = imagecreatefromstring($src);
			    	$dst_r = ImageCreateTrueColor($imagewidth,$imageheight);
			    	imagecopyresampled($dst_r,$img_r,0,0,0,0,$imagewidth,$imageheight,$newimagewidth,$newimageheight);
			
					if(sizeof($fileInfo) > 0){
				
						$type = $fileInfo["type"];
						$tmpName = $fileInfo["tmp_name"];
						
						if (isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
						{
							$name = "img".$authUserNamespace->admintutorid;
						}else{
							$name = "img".$img_tutor_id;
						}
						
						$authUserNamespace->latest_uploaded_image_no = "1";
						$imagefinaltype = explode("/",$type);
						if (isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
						{
							$authUserNamespace->uploaded_image_edit_name = $name.".".$imagefinaltype[1];
							$authUserNamespace->uploaded_image_edit_name1 = $name."small.".$imagefinaltype[1];

						}else{
							$authUserNamespace->uploaded_image_name = $name.".".$imagefinaltype[1];
							$authUserNamespace->uploaded_image_name1 = $name."small.".$imagefinaltype[1];
						}
						imagejpeg($dst_r,dirname(dirname(dirname(__FILE__)))."/docs/".$name.".".$imagefinaltype[1],$jpeg_quality);
					
					}
					
					$this->_redirect('/uploadimagenew/uploadpicturestep2');
				}				
				}
			}
		}
	}
	
	public function uploadpicturestep2Action(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		if (isset($authUserNamespace->admintutorid) && $authUserNamespace->admintutorid!="")
		{
			$img_tutor_id= $authUserNamespace->admintutorid;
			$this->view->id = $img_tutor_id;
		}else{
		$img_tutor_id=$this->_request->getParam("id");
		$this->view->id = $img_tutor_id;
		}
		//echo "<script>parent.Mediabox.close();</script>";
	}
	
	public function showcropimageAction(){
		
		$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
		$this->_helper->layout()->disableLayout();
		
	}
	
	public function saveimageAction(){
		
			$authUserNamespace = new Zend_Session_Namespace('Skillzot_Auth');
			$this->_helper->viewRenderer->setNoRender(true);
			$this->_helper->layout()->disableLayout();
			$tutorCourseobj = new Skillzot_Model_DbTable_Tutorskillcourse();
			if($this->_request->isPost()){
				
				if(isset($authUserNamespace->uploaded_image_name) && $authUserNamespace->uploaded_image_name!=""){
				
				$jpeg_quality = 90;
				$x=$this->_request->getParam("sizeX");
				$y=$this->_request->getParam("sizeY");
				$w=$this->_request->getParam("sizeW");
				$h=$this->_request->getParam("sizeH");
				
				$src=file_get_contents(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->uploaded_image_name);
	    		$img_r = imagecreatefromstring($src);
	    		$dst_r = ImageCreateTrueColor($w,$h);
	    		imagecopyresampled($dst_r,$img_r,0,0,$x,$y,$w,$h,$w,$h);
    			imagejpeg($dst_r,dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->uploaded_image_name1,$jpeg_quality);
				
				//unset($authUserNamespace->uploaded_image_name);
				//unset($authUserNamespace->uploaded_image_name1);
						
   				exit;
				}elseif(isset($authUserNamespace->uploaded_image_edit_name) && $authUserNamespace->uploaded_image_edit_name!=""){
				
				$jpeg_quality = 90;
				$x=$this->_request->getParam("sizeX");
				$y=$this->_request->getParam("sizeY");
				$w=$this->_request->getParam("sizeW");
				$h=$this->_request->getParam("sizeH");
				
				$src=file_get_contents(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->uploaded_image_edit_name);
	    		$img_r = imagecreatefromstring($src);
	    		$dst_r = ImageCreateTrueColor($w,$h);
	    		imagecopyresampled($dst_r,$img_r,0,0,$x,$y,$w,$h,$w,$h);
    			imagejpeg($dst_r,dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->uploaded_image_edit_name1,$jpeg_quality);
				
				//unset($authUserNamespace->uploaded_image_name);
				//unset($authUserNamespace->uploaded_image_name1);
				if (isset($authUserNamespace->courseIdforsave) && $authUserNamespace->courseIdforsave!="")
				{
					if (isset($authUserNamespace->uploaded_image_edit_name1) && $authUserNamespace->uploaded_image_edit_name1!="")
					{
						$image_typearr = explode(".",$authUserNamespace->uploaded_image_edit_name1);
						$image_type = "image/".$image_typearr[1];
						$getCropimagecontent=file_get_contents(dirname(dirname(dirname(__FILE__)))."/docs/".$authUserNamespace->uploaded_image_edit_name1);
					}else{
						$image_type = "";
						$getCropimagecontent="";
					}
					$data = array("tutor_class_image_type"=>$image_type,"tutor_class_image_content"=>$getCropimagecontent);
					$tutorCourseobj->update($data,"id='$authUserNamespace->courseIdforsave'");
					unset($authUserNamespace->courseIdforsave);
				}		
   				exit;
				
				}
				
	    }
	
	}
	

}
?>