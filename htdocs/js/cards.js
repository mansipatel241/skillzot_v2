
$(document).ready(function(){
	$("#imageslider div.content").myslider({ 	itemwidth: 217,
										   		visibleitems: 4
											},
											"#imageslider div.left span",
											"#imageslider div.right span");

	$("#debitcards").mouseenter(function(){ $("#debitcardstooltip").show();	 });
	$("#debitcards").mouseleave(function(){ $("#debitcardstooltip").hide();	 });
	$("#prepaidcards").mouseenter(function(){ $("#prepaidcardstooltip").show();	 });
	$("#prepaidcards").mouseleave(function(){ $("#prepaidcardstooltip").hide();	 });
	$("#commercialcards").mouseenter(function(){ $("#commercialcardstooltip").show();	 });
	$("#commercialcards").mouseleave(function(){ $("#commercialcardstooltip").hide();	 });
	$("#threesteps").mouseenter(function(){ $("#threestepstooltip").show();	 });
	$("#threesteps").mouseleave(function(){ $("#threestepstooltip").hide();	 });

	$(".hastooltip2").mouseenter(function(){ 
		$(this).addClass("relative");
		$(this).find(".tooltip2").show();	 
	});
	$(".hastooltip2").mouseleave(function(){ 
		$(this).removeClass("relative");
		$(this).find(".tooltip2").hide();
	});
	
	$("#cardstooltips .tooltip").hide();

	$("#needhelp").click(function(e){
		$("#frmneedhelp").fadeIn("fast", function(){
			$(document).bind("click", hidefrmNeedHelpOnClickout);
		});
		e.preventDefault();
	});

	$("#frmneedhelp .btnfrmclose").click(hidefrmNeedHelp);
	
	function hidefrmNeedHelp()
	{
		$("#frmneedhelp").fadeOut("fast", function(){
			$("#frmneedhelp input").attr("value", "");
		});
		$(document).unbind("click");
	}

	function hidefrmNeedHelpOnClickout(e)
	{
		if(!$(e.target).parents("#frmneedhelp").length)
			hidefrmNeedHelp();
	}
	
//	$("#imageslider img").mouseenter(function(){
//		$(this).animate({width: 156, height: 125, "marginLeft": "-8"}, "fast");
//	});
//
//	$("#imageslider img").mouseleave(function(){
//		$(this).animate({width: 140, height: 109, "marginLeft": "0"}, "fast");
//	});
});
