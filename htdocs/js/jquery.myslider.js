// JavaScript Document

$.fn.myslider = function(params, btnleft, btnright) {
	var me = this;
	me.list = me.find("ul");
	me.itemcount = me.find("li").length;
	me.itemwidth = params.itemwidth;
	me.visibleitems = params.visibleitems;
	me.slidelefton = false;
	me.sliderighton = false;

	(function a() {
		if(me.sliderighton && me.allowSlideRight()) {			
			me.list.css("margin-left", (parseInt(me.list.css("margin-left")) - 5) + "px");
		}
		if(me.slidelefton && me.allowSlideLeft()) {
			me.list.css("margin-left", (parseInt(me.list.css("margin-left")) + 5) + "px");
		} 
		
		setTimeout(a, 15);
	})();
	
	me.allowSlideLeft = function() {
		return parseInt(me.list.css("margin-left")) < 0;
	};
	
	me.allowSlideRight = function() {
		return (parseInt(me.list.css("margin-left")) * -1) < (me.itemwidth * (me.itemcount - me.visibleitems));
	};	
	
	jQuery(btnleft).mouseenter(function(){
		me.slidelefton = true;
	});

	jQuery(btnleft).mouseleave(function(){
		me.slidelefton = false;
	});

	jQuery(btnright).mouseenter(function(){
		me.sliderighton = true;
	});

	jQuery(btnright).mouseleave(function(){
		me.sliderighton = false;
	});

	return me;
};