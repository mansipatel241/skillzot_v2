/*mini box*/
$(document).ready(function(){
	var bodywidth = $(window).width();
	var bodyheight = $(window).height();
	
	$(window).resize(function() {
		bodywidth = $(window).width();
		bodyheight = $(window).height();
		getcordinate(picnumber);
	});
	$('.eachphoto').click(function(){
		getAlldata($(this));
	});
	var picnumber = 0;
	var photolinkArr = [];
	var titleArr = [];
 	var w = 0;
	var h = 0;
	var a = 0;
	var topv = 0;
	var left = 0;
	var minibox = "";
	var flagw = 0;
	var flagh = 0;
	function getAlldata(obj)
	{	
		var rel = $(obj).attr('rel');
		var title = $(obj).next('.imagetitle').text();
		$('.imagethumb').each(function(){
			photolinkArr.push($(this).children('.eachphoto').attr('rel'));
			titleArr.push($(this).children('.eachphoto').next('input').val());
		});
		for(i=0;i<photolinkArr.length;i++)
		{
			if($(obj).attr('rel') == photolinkArr[i])
			{
				picnumber = i;	
			}
		}
		minibox = '<div id="photoboxCover" class="photobox"></div><div id="photoHolder" class="photobox"><span class="cancel"><span id="cancelphoto"> </span></span><img class="responsiveImg" id="responsiveImg" src="'+rel+'"/><button id="pre" class="photonxtprevbtn"><!--&lt;--></button><button id="nxt" class="photonxtprevbtn"><!--&gt;--></button><div class="imagetitle" id="imagetitle"><span class="myt"></span><div class="number"><span id="cno"></span><span> of </span><span id="tno">'+photolinkArr.length+'</span></div></div></div>';
		getcordinate(picnumber);
		$('body').append(minibox);
	}
	function getcordinate(v)
	{
		bodywidth = $(window).width();
		bodyheight = $(window).height();
		var imgobj = new Image();
		var isIE = (navigator.appVersion.toLowerCase().indexOf('msie') > 0);
		if (isIE)
		{
			imgobj.attachEvent('onload', myfunction);
		}
		else
		{
			imgobj.addEventListener('load', myfunction);
		}
		var x = picnumber+1;
		imgobj.src = photolinkArr[v];
		function myfunction() {
			w = imgobj.width;
			h = imgobj.height;
			a = h/w;
			if(w > h)
			{
				if(w > bodywidth)
				{
					getwidth();
				}
				else
				{
					if(h > bodyheight)
					{
						getheight();
					}
					else
					{
						flagw = 1;
						flagh = 1;
					}
				}
			}
			else if(h > w)
			{
				if(h > bodyheight)
				{
					getheight();
				}
				else
				{
					if(w > bodywidth)
					{
						getwidth();
					}
					else
					{
						flagw = 1;
						flagh = 1;
					}
				}
			}
			else/*w == h*/
			{
				if(w > bodywidth)
				{
					getwidth();
				}
				else
				{
					if(h > bodyheight)
					{
						getheight();
					}
					else
					{
						flagw = 1;
						flagh = 1;
					}
				}
			}
			if(imgobj.width < 550)
			{
				getwidth();
				$('#photoHolder').children('img').removeClass('responsiveImg').css('margin-top',(h/2-imgobj.height/2)+'px');
			}
			else
			{
				$('#photoHolder').children('img').addClass('responsiveImg').css('margin-top','0px');
			}
			if(flagw == 1 || flagh ==1){
				topv = (bodyheight-h)/2;
				left = (bodywidth-w)/2;
				$('#photoHolder').css({'height':h+'px','width':w+'px','top':topv+'px','left':left+'px'});
				$('#photoHolder').children('img').attr('src',photolinkArr[v]);/*.css('opacity','0').animate({'opacity':'1'},300)*/
				
				$('#photoHolder').children('#imagetitle').find('.myt').html(titleArr[v]);
				$('#photoHolder').children('#imagetitle').children('.number').find('#cno').html(parseInt(x));
				$('#photoHolder').children('#imagetitle').find('.myt').width($('#photoHolder').children('#imagetitle').width()-$('#photoHolder').children('#imagetitle').children('.number').width()-40);
				flagw = 0;
				flagh = 0;	
			}
			else
			{
				getcordinate(picnumber);
			}
		};
	}
	function getwidth()
	{
			w = bodywidth-50;
			h = a*w;
			if(h > bodyheight)
			{
				getheight();
			}
			else
			{
				flagh=1;	
				return;
			}
	}
	function getheight()
	{
			h = bodyheight-50;
			w = h/a;
			if(w > bodywidth)
			{
				getwidth();
			}
			else
			{
				flagw=1;
				return;
			}
	}
	$('#pre').live('click', function(){
		if(picnumber > 0)
		{
			picnumber--;
		}
		else
		{
			picnumber = photolinkArr.length-1;
		}
		getcordinate(picnumber);
	});
	$('#nxt').live('click', function(){
		if(picnumber < photolinkArr.length-1)
		{
			picnumber++;
		}
		else
		{
			picnumber = 0;
		}
		getcordinate(picnumber);
	});
	$('#photoboxCover,#cancelphoto').live('click',function(){
			$('#photoHolder').css({'height':'0px','width':'0px','top':'0px','left':'0px'});		
			picnumber = 0;
			photolinkArr = [];
			titleArr = [];
		 	w = 0;
			h = 0;
			a = 0;
			topv = 0;
			left = 0;
			minibox = "";
			flagw = 0;
			flagh = 0;
			$('.photobox').remove();
	});
});	
	/*mini box*/